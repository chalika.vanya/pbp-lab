Chalika Vanya Resya 
2006596711
PBP - B

1. Apakah perbedaan antara JSON dan XML?
    JSON adalah singkatan dari _JavaScript Object Notation_. JSON memiliki format _text_ sehingga mudah dibaca. JSON biasanya digunakan dalam pentransmisian data pada server dan aplikasi web. JSON secara umum terdiri atas _key_ dan _value_ yang kemudian akan membentuk pasangan _key_ dan _value_.

    XML adalah singkatan dari _eXtensible Markup Language_. XML lebih berfokus pada data secara keseluruhan dibandingkan dengan tampilan data tersebut. Data pada XML bersifat _self-descriptive_ dimana data tersebut sudah terstruktur dengan baik sehingga kita tidak perlu membangun struktur dari data tersebut ketika menyimpan data. 

    Beberapa perbedaan mendasar antara JSON dan XML adalah JSON berbasis JavaScript, sedangkan XML merupakan bagian atau turunan dari SGML (_Standard Generalized Markup Language_). Selain itu, apabila dilihat dari struktur dokumen antara JSON dan XML, dokumen JSON lebih mudah dibaca dan dipahami dibandingkan dokumen XML. Masih berbicara tentang struktur dokumen pada JSON maupun XML, struktur dokumen pada XML harus memiliki _opening tag_ dan juga _closing tag_, sedangkan JSON tidak memiliki _closing tag_. Untuk sistem _encoding_, JSON hanya mendukung sistem UTF-8 encoding, sedangkan XML mendukung, tidak hanya UTF-8 encoding, tetapi juga beberapa sistem _encoding_ lainnya. 

2. Apakah perbedaan antara HTML dan XML?
    HTML adalah singkatan dari _Hyper Text Markup Language_. HTML merupakan _standard Markup Language_ dalam membangun tampilan web. HTML memiliki berbagai elemen yang dapat digunakan dalam menampilkan informasi pada laman web. 

    Walaupun HTML dan XML merupakan _markup language_, keduanya tetap memiliki beberapa perbedaan. Salah satu perbedaannya adalah mengenai fungsinya, yaitu HTML berfokus kepada menampilkan data pada laman web, sedangkan XML lebih berfokus kepada bagaimana data tersebut disimpan serta dikirimkan. Untuk strukturnya sendiri, elemen pada HTML tidak semuanya memiliki _closing tag_, seperti element br, sedangkan struktur dokumen pada XML wajib memiliki _opening tag_ dan juga _closing tag_. Berdasarkan tipenya, HTML hanya dapat membuat halaman web yang bertipe _static_ karena hanya digunakan dalam menampilkan data pada laman web, sedangkan XML bersifat _dynamic_ karena digunakan dalam mengirimkan data.


Referensi:
- https://developers.squarespace.com/what-is-json
- https://whatis.techtarget.com/definition/XML-Extensible-Markup-Language
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://www.w3schools.com/html/html_intro.asp
- https://www.javatpoint.com/html-vs-xml
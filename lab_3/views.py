from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login")
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login")
def add_friend(request):
    response = {}

    # create object of form
    form = FriendForm(request.POST or None)

    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-3')

    response['form']= form
    return render(request, "lab3_form.html", response)
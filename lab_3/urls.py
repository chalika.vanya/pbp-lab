from django.urls import path
from .views import add_friend, index
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add'),
    path('admin/login/', auth_views.LoginView.as_view()),
]
import 'package:flutter/material.dart';

class Navbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 1800) {
          return DesktopNavbar();
        } else if (constraints.maxWidth > 1000 && constraints.maxWidth < 1800) {
          return DesktopNavbar();
        } else {
          return MobileNavbar();
        }
      },
    );
  }
}

class DesktopNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Image(
              image: AssetImage("assets/images/stu-do-list.png"),
              width: 50,
              height: 50,
            ),
            Row(
              children: <Widget>[
                Text(
                  "Agenda",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Ask A Mentor",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Jadwal Belajar Bareng",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Notes",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Schedule Kuliah",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Study Communities",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Video Playlist",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  "Saran",
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: 20,
                ),
                MaterialButton(
                  color: Color.fromARGB(255, 55, 126, 243),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  onPressed: () {},
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    child: Text(
                      "Log Out",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class MobileNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
      child: Container(
        child: Column(children: <Widget>[
          Image(
            image: AssetImage("assets/images/stu-do-list.png"),
            width: 50,
            height: 50,
          ),
        ]),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'nav_drawer_header.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          NavigationHeader(),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.view_agenda_outlined,
            title: "Agenda",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.person,
            title: "Ask a Mentor",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.calendar_today,
            title: "Jadwal Belajar Bareng",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.book,
            title: "Notes",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.list_alt,
            title: "Schedule Kuliah",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.people,
            title: "Study Communities",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.video_library,
            title: "Video Playlist",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.feedback,
            title: "Saran",
            onTilePressed: () {},
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Log out",
            onTilePressed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

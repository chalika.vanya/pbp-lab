import 'package:coba_flutter/HomePage/home_page.dart';
import 'package:flutter/material.dart';
import 'nav_drawer_header.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          NavigationHeader(),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.view_agenda_outlined,
            title: "Agenda",
            onClicked: () => selectedItem(context, 0),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.person,
            title: "Ask a Mentor",
            onClicked: () => selectedItem(context, 1),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.calendar_today,
            title: "Jadwal Belajar Bareng",
            onClicked: () => selectedItem(context, 2),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.book,
            title: "Notes",
            onClicked: () => selectedItem(context, 3),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.list_alt,
            title: "Schedule Kuliah",
            onClicked: () => selectedItem(context, 4),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.people,
            title: "Study Communities",
            onClicked: () => selectedItem(context, 5),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.video_library,
            title: "Video Playlist",
            onClicked: () => selectedItem(context, 6),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.feedback,
            title: "Saran",
            onClicked: () => selectedItem(context, 7),
          ),
          SizedBox(height: 20),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Log out",
            onClicked: () => selectedItem(context, 8),
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onClicked;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onClicked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onClicked,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

void selectedItem(BuildContext context, int index) {
  Navigator.of(context).pop();
  switch (index) {
    case 6:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MyHomePage(),
      ));
      break;
  }
}

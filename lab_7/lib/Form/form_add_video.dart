import 'package:flutter/material.dart';

class FormAddVideo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormAddVideoState();
  }
}

class FormAddVideoState extends State<FormAddVideo> {
  String? _title = '';
  String? _url = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTitle() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "1 A.M Study Session 📚 - [lofi hip hop/chill beats]",
        labelText: "Masukkan judul",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Judul tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _title = value!;
      },
    );
  }

  Widget _buildURL() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "https://www.youtube.com/embed/lTRiuFIWV54",
        labelText: "Masukkan tautan (embedded link)",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      keyboardType: TextInputType.url,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tautan tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _url = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildTitle(),
            SizedBox(height: 50),
            _buildURL(),
            SizedBox(height: 50),
            MaterialButton(
              color: Color.fromARGB(255, 11, 94, 215),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: Text(
                'Tambah',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              padding:
                  const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              onPressed: () {
                if (!_formKey.currentState!.validate()) {
                  return;
                }
                _formKey.currentState!.save();
                print(_title);
                print(_url);

                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Video berhasil ditambahkan!')),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

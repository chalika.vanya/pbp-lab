import 'package:flutter/material.dart';
import 'package:coba_flutter/screen/drawer.dart';

import 'form_add_video.dart';

class FormAddPage extends StatelessWidget {
  static const routeName = '/form-video';
  const FormAddPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerScreen(),
      appBar: AppBar(
        title:
            Image.asset("assets/images/stu-do-list.png", width: 50, height: 50),
        centerTitle: true,
        backgroundColor: Color.fromARGB(255, 33, 37, 41),
      ),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Text(
              "Tambah Video",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 50,
                  fontWeight: FontWeight.bold),
            ),
            FormAddVideo()
          ],
        ),
      )),
    );
  }
}

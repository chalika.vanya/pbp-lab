import 'package:flutter/material.dart';
import 'Form/form_add_video_page.dart';
import 'HomePage/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stu-do-list',
      theme: ThemeData(primarySwatch: Colors.blue, fontFamily: "Poppins"),
      home: MyHomePage(),
      initialRoute: MyHomePage.routeName,
      routes: {
        MyHomePage.routeName: (context) => MyHomePage(),
        FormAddPage.routeName: (context) => FormAddPage(),
      },
    );
  }
}
